#include "Functions.h"

#include <VHBL.h>

void readEbootName(const char * filename, const char * backup,	char* name) {

		typedef struct {
			char signature[4];
			int version;
			int offset[8];
	}PBPHeader;

	typedef struct {
		char signature[4];
		int version;
		int offset_key_table;
		int offset_values_table;
		int num_entries; 
	}PSFHeader;

	int fid; // file id
	unsigned char index_table[MAX_INDEX_TABLE_SIZE][16];
	PBPHeader pbpHeader;    
	PSFHeader psfHeader;

	fprintf(stderr, "File is [%s]\n",filename);
	if(!(fid = sceIoOpen(filename, PSP_O_RDONLY, 0777)))
	{
		//LOGSTR0("File is NULL");
		return;
	} 

	/* PBP SECTION */
	sceIoRead(fid, &pbpHeader, sizeof(pbpHeader)); // read in pbp header
	if(!(pbpHeader.signature[1] == 'P' && pbpHeader.signature[2] == 'B' && pbpHeader.signature[3] == 'P')) 
	{ 
		strcpy(name, backup);
		strcat(name," (Corrupt or invalid PBP)");
		//LOGSTR0("File not a PBP\n");
		sceIoClose(fid);
		return;
	} 

	/* PSF SECTION */
	sceIoLseek(fid, pbpHeader.offset[0] , PSP_SEEK_SET ); // seeks to psf section - first entry in pbp offset table
	sceIoRead(fid,&psfHeader, sizeof(psfHeader));                                                                                       // reads in psf header

	//LOGSTR1("PSF number of Entries [%d]\n", psfHeader.num_entries);
	if (psfHeader.num_entries > MAX_INDEX_TABLE_SIZE)
	{ 
		strcpy(name, backup);
		strcat(name," (Corrupt or invalid PBP)");
		//LOGSTR0("File not a PBP\n");
		sceIoClose(fid);
		return;
	}    
	sceIoRead(fid,&index_table,((sizeof(unsigned char) * 16 ) * psfHeader.num_entries));

	// builds offset [offset_key + Offset_data_value + (data_align * 2)]
	int seek_offset = index_table[(psfHeader.num_entries - 1)][OFFSET_KEY_NAME] + index_table[(psfHeader.num_entries - 1)][OFFSET_DATA_VALUE] +         (index_table[(psfHeader.num_entries - 1)][DATA_ALIGN] * 2);

	// some offset are not even numbers so they read in some off the null padding
	if (seek_offset % 2 > 0)
	{
		seek_offset++;
	}

	//LOGSTR1("PSF using offset [%d]\n", seek_offset);

	sceIoLseek(fid, seek_offset, PSP_SEEK_CUR);
	sceIoRead(fid,name, (sizeof(char) * index_table[(psfHeader.num_entries - 1)][SIZE_OF_VALUE]));

	if (!name[0]) {
		strcpy(name, backup);
	}

	//LOGSTR1("Name is [%s]\n",(u32)name);

	sceIoClose(fid);
}

int not_homebrew(const char * name)
{
	int i;

	if (strlen(name) == 0) return 1;
	//official games are in the form: XXXX12345
	if (strlen(name) != 9) return 0;
	for (i = 4; i < 9; ++i)
	{
		if (name[i] < '0' || name[i] > '9')
			return 0;
	}

	return 1;
}


void GamesViewInit()
{	
	menuData = "ms0:/PSP/GAME";
	eboot = "ms0:/PSP/GAME/";
	SceUID game = sceIoDopen(menuData);
	SceIoDirent entry;

	memset(&folders,0,sizeof(folders));

	int nbFiles = 0;

	if(game > 0)
	{
		while (sceIoDread(game, &entry) > 0 && nbFiles < NB_FOLDERS)
		{
			if (strcmp(".", entry.d_name) == 0 || strcmp("..", entry.d_name) == 0 || not_homebrew(entry.d_name)) 
			{
				memset(&entry, 0, sizeof(SceIoDirent)); 
				continue;
			}

			strcpy(folders[nbFiles], entry.d_name);
			nbFiles++;
			memset(&entry, 0, sizeof(SceIoDirent)); 
		}

		sceIoDclose(game);
	}

	numGames = nbFiles;

	gamestartIndex = 0;

	yPOS = 30;

	height = 227/(numGames/2);

}

void GamesView()
{
	oslDrawFillRect(135,20,455,267,RGBA(15,15,15,100));
	oslDrawString(180,95,folders[gamestartIndex]);
	if(numGames > gamestartIndex + 1)oslDrawString(320,95,folders[gamestartIndex + 1]);
	if(numGames > gamestartIndex + 2)oslDrawString(180,215,folders[gamestartIndex + 2]);
	if(numGames > gamestartIndex + 3)oslDrawString(320,215,folders[gamestartIndex + 3]);
	oslDrawFillRect(465, yPOS, 480,yPOS + height,RGB(0,0,0)); 
	oslDrawString(10,3,eboot);
}

void GamesClick(int index)
{
	strcat(eboot, folders[gamestartIndex+index]);
	strcat(eboot, "/WMENU.PBP");
	//eboot = "ms0:/PSP/SAVEDATA/NPUZ00097SLOT00/EBOOT.PBP";
	exitGame = 1;
}

void GamesScrollDown()
{
	if(numGames > gamestartIndex+2){
		gamestartIndex+=2;

		//Draw scroll bar
		yPOS+=height;
	}


}

void GamesScrollUp()
{
	if(0 <= gamestartIndex-2){
		gamestartIndex-=2;

		yPOS-= height;
	}
}