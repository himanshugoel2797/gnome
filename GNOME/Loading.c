#include "Loading.h"

OSL_IMAGE* bkg;


void startupInit()
{
	bkg = oslLoadImageFilePNG("LOGO.PNG",OSL_IN_RAM,OSL_PF_8888);
	OSL_FONT *font = oslLoadFontFile("CANTAR.PGF");
	oslIntraFontSetStyle(font, 2.0f,WHITE,BLACK,INTRAFONT_ALIGN_LEFT);
	oslSetFont(font);
	
}

//Show the loading screen
void show()
{
	oslClearScreen(0xFFBFBFBF);
	bkg->stretchX = bkg->sizeX/4;
	bkg->stretchY = bkg->sizeY/4;
	
	
	oslDrawImageSimpleXY(bkg,200,10);
	oslDrawString(100,150,"GNOMEnu");
}

//Detailed Initialization
void OSLInit()
{

}
