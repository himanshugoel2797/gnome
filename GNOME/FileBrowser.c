#include "Functions.h"

void FileList()
{
	SceUID game = sceIoDopen(menuData);
	SceIoDirent entry;

	int nbFiles = 0;

	if(game > 0)
	{
		while (sceIoDread(game, &entry) > 0 && nbFiles < NB_FOLDERS)
		{
			if (strcmp(".", entry.d_name) == 0 || strcmp("..", entry.d_name) == 0) 
			{
				memset(&entry, 0, sizeof(SceIoDirent)); 
				continue;
			}

			strcpy(folders[nbFiles], entry.d_name);
			nbFiles++;
			memset(&entry, 0, sizeof(SceIoDirent)); 
		}

		sceIoDclose(game);
	}

	numFile = nbFiles;

	FilesStartIndex = 0;

	yPOS = 30;

	height = 227/(numFile/2);

}


void FileViewInit()
{	

	numFile = 0;
	FilesStartIndex = 0;
	menuData = "ms0:/PSP/GAME";

	FileList();

}

void FileView()
{
	oslDrawFillRect(135,20,455,267,RGBA(15,15,15,100));
	oslDrawString(180,95,folders[gamestartIndex]);
	if(numFile > FilesStartIndex + 1)oslDrawString(320,95,folders[FilesStartIndex + 1]);
	if(numFile > FilesStartIndex + 2)oslDrawString(180,215,folders[FilesStartIndex + 2]);
	if(numFile > FilesStartIndex + 3)oslDrawString(320,215,folders[FilesStartIndex + 3]);
	oslDrawFillRect(465, yPOS, 480,yPOS + height,RGB(0,0,0)); 
	oslDrawString(10,3,menuData);
}

void FileClick(int index)
{
		strcat(menuData,"/");
		strcat(menuData,folders[FilesStartIndex + index]);
		FileList();
		FilesStartIndex = 0;
}

void FileScrollDown()
{
	if(numFile > FilesStartIndex+2){
		FilesStartIndex+=2;

		//Draw scroll bar
		yPOS+=height;
	}


}

void FileScrollUp()
{
	if(0 <= FilesStartIndex-2){
		FilesStartIndex-=2;

		yPOS-= height;
	}
}