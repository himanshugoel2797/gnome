#include <pspkernel.h>


#include "Loading.h"
#include "MenuInit.h"
#include "Common.h"

#include <oslib/oslib.h>

PSP_MODULE_INFO("Internet Browser Test", 0, 1, 0);
PSP_MAIN_THREAD_ATTR(THREAD_ATTR_USER | THREAD_ATTR_VFPU);
PSP_HEAP_SIZE_KB(4*1024);

/* Half Byte Loader is GPL
* Crappy Menu(tm) wololo (One day we'll get something better)
* thanks to N00b81 for the graphics library!
* 
*/

#include <pspsdk.h>
#include <pspkernel.h>
#include <pspdebug.h>
#include <pspctrl.h>
#include <pspdisplay.h>
#include <psppower.h>
#include <string.h>
#include <stdio.h>

#define FOLDERNAME_SIZE 30
#define NB_FOLDERS 40
#define MAXMENUSIZE 17

#define MAX_INDEX_TABLE_SIZE 11 // max number of entries in psf table 
#define MAX_NAME_SIZE 59
#define FILE_BUFFER_SIZE 80

// index table offsets
#define OFFSET_KEY_NAME 0
#define DATA_ALIGN 2
#define DATA_TYPE 3
#define SIZE_OF_VALUE 4
#define SIZE_DATA_AND_PADDING 8
#define OFFSET_DATA_VALUE 12

//initially from eMenu--
typedef struct
{
        unsigned long        APIVersion;
        char       Credits[32];
        char       VersionName[32];
        char       *BackgroundFilename;   // set to NULL to let menu choose.
    char        * filename;   // The menu will write the selected filename there
}       tMenuApi;


char folders[NB_FOLDERS][FOLDERNAME_SIZE] ;

char * cacheFile = "menu.cache";

int currentFile;
int isSet;
void * ebootPath;


//Globals
int runningFlag;

/*
 * hexascii to integer conversion
 */
static int xstrtoi(char *str, int len) {
        int val;
        int c;
        int i;

        val = 0;
    for (i = 0; i < len; i++){
        c = *(str + i);
        fprintf(stderr, "character: %c", c);
                if (c >= '0' && c <= '9') {
                        c -= '0';
                } else if (c >= 'A' && c <= 'F') {
                        c = (c - 'A') + 10;
                } else if (c >= 'a' && c <= 'f') {
                        c = (c - 'a') + 10;
                } else {
                        return 0;
                }
                val *= 16;
                val += c;
        }
        return (val);
}



//init oslib
int initOSLib(){
	oslInit(0);
	oslInitGfx(OSL_PF_8888, 1);
	oslInitAudio();
	oslSetQuitOnLoadFailure(1);
	oslSetKeyAutorepeatInit(40);
	oslSetKeyAutorepeatInterval(10);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main:
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[]){

	char menuEntry[NB_FOLDERS][MAX_NAME_SIZE];
        int menuOffSet = 0;
        char buffer[20];
    char dummy_filename[256];
    strcpy(dummy_filename, "ms0:/PSP/GAME");
    currentFile = 0;

    int settingsAddr = 0;

    if (argc > 1) {
        char * hex = argv[1];
        *(hex + 8 ) = 0; //bug in HBL ?
        fprintf(stderr, "Location: 0x %s\n", hex);
        settingsAddr = xstrtoi(hex, 8);
    }
    fprintf(stderr, " settingsAddr : %d\n", settingsAddr);
    if (settingsAddr) {
        tMenuApi * settings = (tMenuApi *) settingsAddr;
        ebootPath = (void *) settings->filename;
    } else {
        ebootPath = dummy_filename;
    }


	int skip = 0;

	int loadTimer = 60;

	int initMain = 0;

	char message[100] = "";

	initOSLib();
	oslIntraFontInit(INTRAFONT_CACHE_ALL | INTRAFONT_STRING_UTF8); // All fonts loaded with oslLoadIntraFontFile will have UTF8 support


	//Initialize basic startup routines
	startupInit();
	exitGame = 0;

	while(!osl_quit && !exitGame)
	{
		if(!skip){
			oslStartDrawing();
			if(loadTimer >= 0){
				show();
				loadTimer--;
			}
			else
			{
				if(!initMain){
					InitMenu();
					initMain = 1;
				}


				transitWall();
				
			}
		}
		oslEndDrawing();
		oslEndFrame();
		skip = oslSyncFrame();
	}

	//Quit OSL:
	oslEndGfx();

	strcpy(ebootPath, eboot);
	sceKernelExitGame();
	return 0;

}
