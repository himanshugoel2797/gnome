#include "MenuInit.h"


void InitMenu()
{

	
	cross = oslLoadImageFilePNG("CROSS.PNG",OSL_IN_RAM,OSL_PF_8888);
	cross->stretchX=20;
	cross->stretchY=20;

	wall = oslLoadImageFilePNG("WALL.PNG",OSL_IN_VRAM|OSL_SWIZZLED, OSL_PF_8888);
	
	wall->stretchX = 480;
	wall->stretchY = 272;

	mouse = oslLoadImageFilePNG("CURSOR.PNG",OSL_IN_VRAM|OSL_SWIZZLED, OSL_PF_8888);

	mouse->x = 100;
	mouse->y = 100;
	mouse->stretchX = 10;
	mouse->stretchY = 10;


	alphaCon = 255;
	optionView = 0;
	gamesView = 0;

	//Initialize all the menus
	GamesViewInit();

	OSL_FONT *font = oslLoadFontFile("CANTAR.PGF");
	oslIntraFontSetStyle(font, 0.5f,WHITE,WHITE,INTRAFONT_ALIGN_LEFT);

	OSL_FONT *font1 = oslLoadFontFile("CANTAR.PGF");
	oslIntraFontSetStyle(font1, 1.0f,WHITE,WHITE,INTRAFONT_ALIGN_LEFT);

	oslSetFont(font);
}

void transitWall()
{
	if(alphaCon != -1){
		showWall();
		oslDrawFillRect(0,0,480,272,RGBA(0,0,0,alphaCon));
		alphaCon--;
	}else{
		showWall();
	}
}


void showWall()
{
	oslDrawImageXY(wall, 0,0);

	//Draw a bar on top
	oslDrawGradientRect(0,0,480,15,RGB(0,0,0),RGB(0,0,0),RGB(50,50,50),RGB(50,50,50));

	//Draw the text
	oslDrawString(410,2,"Options");
	oslDrawString(3,25, "Games");
	oslDrawString(3,45, "Emulators");
	oslDrawString(3,65, "Applications");
	oslDrawString(3,85, "Books");
	oslDrawString(3,105, "Data");
	oslDrawString(3,125, "Nautilus");

	//Draw the transparent background
	oslDrawFillRect(0,15,130,272,RGBA(56,143,164,100));

	//Draw the scroll bar
	oslDrawFillRect(465,15,480,272, RGBA(56,56,56,200));
	oslDrawFillRect(465,15,480,30, RGBA(16,16,16,255));
	oslDrawFillRect(465,257,480,272, RGBA(16,16,16,255));

	//Show the menus
	if(gamesView)GamesView();
	if(emulatorsView)EmulatorsView();
	if(applicationsView)ApplicationsView();
	if(booksView)BooksView();
	if(dataView)DataView();
	if(nautilus)FileView();

	//If in option view, show the option Menu
	if(optionView)showOption();
	Mouse();

	//Draw the mouse
	oslDrawImage(mouse);
}

void Mouse()
{
	oslReadKeys();

	if(osl_keys->analogX >= 50)
		mouse->x+= osl_keys->analogX/20;
	if(osl_keys->analogX <= -50)
		mouse->x+=osl_keys->analogX/20;

	if(osl_keys->analogY >= 50)
		mouse->y+= osl_keys->analogY/20;
	if(osl_keys->analogY <= -50)
		mouse->y+= osl_keys->analogY/20;

	if(mouse->x <= 0)mouse->x = 0;
	if(mouse->y <= 0)mouse->y = 0;
	if(mouse->x >= 480)mouse->x = 480;
	if(mouse->y >= 272)mouse->y = 272;
	if(osl_keys->pressed.cross)mouseClick();
	if(osl_keys->pressed.triangle)mouseDetail();
	if(osl_keys->pressed.down)scrollDown();
	if(osl_keys->pressed.up)scrollUp();
	mouseOver();
}


void mouseOver()
{
	if(mouse->x >410 && mouse->y < 15 && !optionView)
	{
		oslDrawFillRect(407,0,480,15, RGBA(150,150,150,50));
	}

	if(mouse->x < 130 && !optionView)
	{
		if(mouse->y > 20 && mouse->y < 40)oslDrawFillRect(0,20,130,40,RGBA(56,143,164,200));
		if(mouse->y > 40 && mouse->y < 60)oslDrawFillRect(0,40,130,60,RGBA(56,143,164,200));
		if(mouse->y > 60 && mouse->y < 80)oslDrawFillRect(0,60,130,80,RGBA(56,143,164,200));
		if(mouse->y > 80 && mouse->y < 100)oslDrawFillRect(0,80,130,100,RGBA(56,143,164,200));
		if(mouse->y > 100 && mouse->y < 120)oslDrawFillRect(0,100,130,120,RGBA(56,143,164,200));
		if(mouse->y > 120 && mouse->y < 140)oslDrawFillRect(0,120,130,140,RGBA(56,143,164,200));
	}

	if(mouse->y > 25 && mouse->y < 110 && mouse->x > 160 && mouse->x < 290 && !optionView)oslDrawFillRect(160,25,290,110,RGBA(30,30,30,150));
	if(mouse->y > 25 && mouse->y < 110 && mouse->x > 300 && mouse->x < 430 && !optionView)oslDrawFillRect(300,25,430,110,RGBA(30,30,30,150));
	if(mouse->y > 145 && mouse->y < 230 && mouse->x > 160 && mouse->x < 290 && !optionView)oslDrawFillRect(160,145,290,230,RGBA(30,30,30,150));
	if(mouse->y > 145 && mouse->y < 230 && mouse->x > 300 && mouse->x < 430 && !optionView)oslDrawFillRect(300,145,430,230,RGBA(30,30,30,150));

}

void mouseDetail()
{

}

void mouseClick()
{
	if(mouse->x >410 && mouse->y < 15 && !optionView)
	{
		optionView = 1;
	}

	if(mouse->x > 440 && mouse->x < 460 && mouse->y > 20 && mouse->y < 40 && optionView)
	{
		optionView = 0;
	}

	if(mouse->x > 465 && mouse->y > 262 && !optionView)
	{
		scrollDown();
	}

	if(mouse->x > 465 && mouse->y < 25 && mouse->y > 15 && !optionView)
	{
		scrollUp();
	}

	if(mouse->x < 130 && !optionView)
	{
		if(mouse->y > 20 && mouse->y < 40){resetAll(); gamesView = 1; GamesViewInit();}
		if(mouse->y > 40 && mouse->y < 60){resetAll(); emulatorsView = 1; EmulatorsViewInit();}
		if(mouse->y > 60 && mouse->y < 80){resetAll(); applicationsView = 1; ApplicationsViewInit();}
		if(mouse->y > 80 && mouse->y < 100){resetAll(); booksView = 1; BooksViewInit();}
		if(mouse->y > 100 && mouse->y < 120){resetAll(); dataView = 1; DataViewInit();}
		if(mouse->y > 120 && mouse->y < 140){resetAll(); nautilus = 1; FileViewInit();}
	}

	if(mouse->y > 25 && mouse->y < 110 && mouse->x > 160 && mouse->x < 290 && !optionView && gamesView)GamesClick(0);
	if(mouse->y > 25 && mouse->y < 110 && mouse->x > 300 && mouse->x < 430 && !optionView && gamesView)GamesClick(1);
	if(mouse->y > 145 && mouse->y < 230 && mouse->x > 160 && mouse->x < 290 && !optionView && gamesView)GamesClick(2);
	if(mouse->y > 145 && mouse->y < 230 && mouse->x > 300 && mouse->x < 430 && !optionView && gamesView)GamesClick(3);

	if(mouse->y > 25 && mouse->y < 110 && mouse->x > 160 && mouse->x < 290 && !optionView && nautilus)FileClick(0);
	if(mouse->y > 25 && mouse->y < 110 && mouse->x > 300 && mouse->x < 430 && !optionView && nautilus)FileClick(1);
	if(mouse->y > 145 && mouse->y < 230 && mouse->x > 160 && mouse->x < 290 && !optionView && nautilus)FileClick(2);
	if(mouse->y > 145 && mouse->y < 230 && mouse->x > 300 && mouse->x < 430 && !optionView && nautilus)FileClick(3);
}

void showOption()
{
	oslDrawFillRect(0,0,480,272,RGBA(15,15,15,150));

	oslDrawImageXY(cross,440,20);
}


void scrollUp()
{
	oslDrawFillRect(465,15,480,30, RGBA(0,0,0,255));
	if(gamesView)GamesScrollUp();
	if(nautilus)FileScrollUp();
}

void scrollDown()
{

	oslDrawFillRect(465,257,480,272, RGBA(0,0,0,255));
	if(gamesView)GamesScrollDown();
	if(nautilus)FileScrollDown();
}


void resetAll()
{
	gamesView = 0;
	emulatorsView = 0;
	applicationsView = 0;
	booksView = 0;
	dataView = 0;
	nautilus = 0;
}