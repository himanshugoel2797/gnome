#ifndef _FUNCTIONS_H_
#define _FUNCTIONS_H_

#include <oslib\oslib.h>
#include <stdio.h>
#include <pspkernel.h>

#include "Common.h"

#define FOLDERNAME_SIZE 30
#define NB_FOLDERS 40
#define MAXMENUSIZE 17

#define MAX_INDEX_TABLE_SIZE 11 // max number of entries in psf table 
#define MAX_NAME_SIZE 59
#define FILE_BUFFER_SIZE 80

// index table offsets
#define OFFSET_KEY_NAME 0
#define DATA_ALIGN 2
#define DATA_TYPE 3
#define SIZE_OF_VALUE 4
#define SIZE_DATA_AND_PADDING 8
#define OFFSET_DATA_VALUE 12

char folders[NB_FOLDERS][FOLDERNAME_SIZE];
char exfilePaths[NB_FOLDERS][MAX_NAME_SIZE];
char EbootName[NB_FOLDERS][MAX_NAME_SIZE];

int yPOS;
int height;

void GamesViewInit();
void GamesView();
void GamesScrollDown();
void GamesScrollUp();
void GamesClick(int index);
int gamestartIndex;
int numGames;

void ApplicationsViewInit();
void ApplicationsView();
void ApplicationsScrollDown();
void ApplicationsScrollUp();
int ApplicationstartIndex;
int numApplications;

void BooksViewInit();
void BooksView();
void BooksScrollDown();
void BooksScrollUp();
int BookstartIndex;
int numBooks;

void EmulatorsViewInit();
void EmulatorsView();
void EmulatorsScrollDown();
void EmulatorsScrollUp();
int EmulatorstartIndex;
int numEmulators;

void DataViewInit();
void DataView();
void DataScrollDown();
void DataScrollUp();
int DatatartIndex;
int numData;

char* menuData;
void FileList();
void FileViewInit();
void FileView();
void FileClick(int index);
void FileScrollUp();
void FileScrollDown();
int FilesStartIndex;
int numFile;

#endif

