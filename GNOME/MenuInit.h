#ifndef _MENU_INIT_H_
#define _MENU_INIT_H_

#include <oslib\oslib.h>
#include "Common.h"
#include "Functions.h"

OSL_IMAGE* wall;
OSL_IMAGE* mouse;
OSL_IMAGE* cross;


int gamesView;
int emulatorsView;
int applicationsView;
int booksView;
int dataView;
int nautilus;

int alphaCon;
int optionView;

void InitMenu();
void transitWall();
void  showWall();
void Mouse();
void mouseClick();
void mouseDetail();
void scrollDown();
void scrollUp();
void mouseOver();
void resetAll();
void showOption();

#endif

